// Package is 特定フォルダの画像を取得し配列化
// ファイル名を整形し連番に
// ffmpeg -r 2 -i image_%d.jpg -vf "scale='min(720,iw)':min'(1280,ih)':force_original_aspect_ratio=decrease,pad=720:1280:(ow-iw)/2:(oh-ih)/2" -vcodec libx264 -pix_fmt yuv420p -r 1 out.mp4
// 上記コマンドを実行し、jpgを縦動画にする
package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"strings"
	"time"

	"github.com/labstack/gommon/log"
)

const (
	// ディレクトリ指定（最後のスラッシュはいりません）
	DIR_INPUT = "./input"
	// DIR_INPUT  = "./input"
	DIR_OUTPUT = "./output"
)

func main() {
	// 特定フォルダを見び出しファイルを配列にする
	files, err := ioutil.ReadDir(DIR_INPUT)
	if err != nil {
		log.Fatal(err)
	}

	// ファイル配列から名称を統一性のあるものにする
	for i, file := range files {
		// 拡張子を取得する
		filename := file.Name()
		pos := strings.LastIndex(filename, ".")
		e := filename[pos+1:]

		if e == "jpg" {
			rename(i, file)
		} else {
			log.Infof("JPGではなく、%sなのでスルー", e)
		}
	}

	toMP4()
}

func rename(i int, file os.FileInfo) {
	o := DIR_INPUT + "/" + file.Name()
	n := fmt.Sprintf("%s/f_%d.jpg", DIR_INPUT, i)

	if err := os.Rename(o, n); err != nil {
		log.Errorf("Rename has %s", err)
	}
}

func toMP4() {
	t := time.Now()
	fmt.Println("ffmpeg実行開始")

	var (
		cmd *exec.Cmd
		err error
	)

	// // ディレクトリ移動
	// cmd = exec.Command("cd", "./input")
	// cmd = exec.Command("cd", "../../")
	// cmd.Stderr = os.Stderr
	// cmd.Stdout = os.Stdout
	// err = cmd.Run()
	// if err != nil {
	// 	log.Error(err)
	// }

	// // 移動後パス確認
	// cmd = exec.Command("pwd")
	// cmd.Stderr = os.Stderr
	// cmd.Stdout = os.Stdout
	// err = cmd.Run()
	// if err != nil {
	// 	log.Error(err)
	// }

	// ここのパス指定は面倒でもフルパス
	// + や fmt.Sprintでpathを生成するとエラーでちゃう
	cmd = exec.Command("ffmpeg",
		"-r", "2",
		"-i", "./input/f_%d.jpg",
		"-vf", `scale='min(720,iw)':min'(1280,ih)':force_original_aspect_ratio=decrease,pad=720:1280:(ow-iw)/2:(oh-ih)/2`,
		"-vcodec", "libx264",
		"-pix_fmt", "yuv420p",
		"-r", "1",
		"out.mp4")
	cmd.Stderr = os.Stderr
	cmd.Stdout = os.Stdout

	err = cmd.Run()
	if err != nil {
		log.Error(err)
	}

	fmt.Println("終了")
	tt := time.Now()
	fmt.Printf("経過時間:%v\n", tt.Sub(t))
}
